# Debug Samples

## Basics (sample1)

- Breakpoint basics
	- Set breakpoint
	- View breakpoints
	- Mute breakpoints
	- Start debugger
	- Resume (F9)
- Debugger view
    - Frames, Threads
    - Variables
  		- Static variables
    - Watches
    - Stepping toolbar
    - Restore view
- Stepping
	- Step over
	- Step into
	- Force step into
	- Show execution point
	- Step out
	- Run to cursor
- Frame
	- Drop Frame
	- Force return (getSkillString)
- Variables
	- Set Value (e.g. `getSkill` method and `skillString`)
	- Evaluate expression
	    - Modify `someObjectsList` and `debugElements`
- Memory View
    - Show instances of `ArrayList` and `SomeObject`
    - Filtering on e.g. `this.someField.equals("A1")`
	
## Breakpoints - options and types (sample2)

- Field breakpoint (`Weapon#attackPower`)
    - Field access, field modification
- Method breakpoint (`Weapon#getName`)
	> Note: "Method breakpoints: suspend the program upon entering or exiting the specified method or one of its implementations, allowing you to check the method's entry/exit conditions."
- Exception breakpoint (`NoWeaponDuringAttack`)
	- In Breakpoint list view add exception breakpoint (any or specified)
	- Frames navigation and drop frame to avoid throwing exception
- Stack Trace
  - Get memory dump in a debuging session (`Character.#attack`)
  - Action: `Analyze Stack Trace or Thread Dump`	

### Optional
	
- Breakpoint options
	- Enabled
	- Remove once hit (`Weapon#getName`)
	- Condition (`Character#takeWeapon => weapon.attackPower >= 50`, `DebugApp2#main => isKilled == false|true`)
	- Logging breakpoint
- Watches

## Debugging Lambda Expressions (sample3 with JUnit) 

- Set breakpoint (`ZipFileBrowser#getMatchingEntries L:20`, `ZipFileBrowser#containsText L:23`)
- Step into
	- Set line breakpoint in `ZipFileBrowser#getMatchingEntries L:20`
	- Start debugging and select `Step Into` when breakpoint reached 
	- Set line breakpoint and while debugging step into as many times as needed
- Run to cursor 
	- Set line breakpoint and while debugging run to cursor as many times as needed
- Action: `Trace Current Stream Chain`

## Hot swapping (sample4)

- Simple hot swapping with method change
