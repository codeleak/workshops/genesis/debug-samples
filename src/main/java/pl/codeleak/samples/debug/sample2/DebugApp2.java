package pl.codeleak.samples.debug.sample2;

import pl.codeleak.samples.debug.sample2.character.Character;
import pl.codeleak.samples.debug.sample2.character.GoodCharacter;

public class DebugApp2 {

    public static void main(final String[] args) {

        final Character jonSnow = new GoodCharacter("Jon Snow", 50);
        final NightWatch nightWatch = new NightWatch();

        boolean isKilled = nightWatch.attack(jonSnow);
        System.out.println(jonSnow.getName() + " is " + (isKilled ? "dead" : "safe"));

    }
}
